# 8-5 cities page 137

# define funtion
def describe_city():
    """get city & country and write a sentence back"""
    print ("Name a city: ")
    city = input(" ")
    print (f" What country is that in? ")
    country = input(" ")
    print (f"\n You said {city.title()} is in {country.title()}. \n")

# call funtion
n = 0
while n < 3:
    describe_city()
    n = n + 1