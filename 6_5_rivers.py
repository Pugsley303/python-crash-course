# 6-5 rivers page 105

rivers_0 = {'missouri': 'usa', 'nile': 'egypt', 'yukon': 'canada'}
for key, value in rivers_0.items():
    print(f"River: {key.title()}")

for key, value in rivers_0.items():
    print(f"Value: {value.title()}")
print("\n")

# 6-6 fav programming languages poll
persons = ['india', 'juliet', 'kilo', 'lima', 'mike']
pollers_0 = {'india': 'c', 'juliet': 'python', 'kilo': 'ruby', 'lima': 'java'}
for person in persons:
    if person not in pollers_0.keys():
        print(f"{person.title()} please take our poll.")
