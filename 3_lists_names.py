# practicing with lists page 42

# 3-4
names = ['Ann', 'Sarah', 'Jaclynn', 'Megan', 'Erin']
print(f"\nHello {names[0]} would you like to go to dinner?")
print(f"\nHello {names[1]} would you like to go to dinner?")
print(f"\nHello {names[2]} would you like to go to dinner?")
print(f"\nHello {names[3]} would you like to go to dinner?")
print(f"\nHello {names[4]} would you like to go to dinner?")
# 3-5
print(f"\nBummer {names[3]} can't make it")
del names[3]
names.insert(3, 'Janet')
print(f"\nHello {names[0]} would you like to go to dinner?")
print(f"\nHello {names[1]} would you like to go to dinner?")
print(f"\nHello {names[2]} would you like to go to dinner?")
print(f"\nHello {names[3]} would you like to go to dinner?")
print(f"\nHello {names[4]} would you like to go to dinner?")
# 3-6
print("\nFound a bigger table!")
names.insert(0, 'Heather')
names.insert(3, 'Missy')
names.append('Shannon')
print(f"\nHello {names[0]} would you like to go to dinner?")
print(f"\nHello {names[1]} would you like to go to dinner?")
print(f"\nHello {names[2]} would you like to go to dinner?")
print(f"\nHello {names[3]} would you like to go to dinner?")
print(f"\nHello {names[4]} would you like to go to dinner?")
print(f"\nHello {names[5]} would you like to go to dinner?")
print(f"\nHello {names[6]} would you like to go to dinner?")
print(f"\nHello {names[7]} would you like to go to dinner?")
# 3-7 popping names off the list
print(f"\nSorry, but now they say I can only have 2!")
popped_name = names.pop()
print(f"\nSorry {popped_name}, but you aint going")
popped_name = names.pop()
print(f"\nSorry {popped_name}, but you aint going")
popped_name = names.pop()
print(f"\nSorry {popped_name}, but you aint going")
popped_name = names.pop()
print(f"\nSorry {popped_name}, but you aint going")
popped_name = names.pop()
print(f"\nSorry {popped_name}, but you aint going")
popped_name = names.pop()
print(f"\nSorry {popped_name}, but you aint going")
print(f"\n{names[0]} would you still like to go?")
print(f"\n{names[1]} would you still like to go?")
del names[0]
print(f"\nNot empty yet.")
del names[0]
print("\nEmpty now.")