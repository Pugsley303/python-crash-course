# 7-3 multiples of 10 
print("Demonstrating user input of a number and check if it is a multiple of 10. ")
num = input("Please provide a number: ")

# convert it from text to an integer
num = int(num)

# check if it is divisible by 10
if num % 10 == 0:
    print(f"\n{num} is a multiple of 10!")
else:
    print(f"\n{num} is not a multiple of 10!")