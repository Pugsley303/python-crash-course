# 6-1 dictionaries - person page 99 
people_0 = {'first': 'corey', 'last': 'mcdonald', 'age': '50', 'city': 'denver'}
print(people_0)

# 6-2 favorite numbers
peepsfavs_0 = {'corey': '0', 'ann': '1', 'george': '2', 'edward': '3', 'frank': '4'}
print(peepsfavs_0)

# 6-3 glossary / glossary 2 page 105
glossary_0 = {'print': 'print', 'if': 'if', 'elif': 'elif', 'else': 'else', 'for': 'for'}
for key, value in glossary_0.items():
    print(f"\nKey: {key}")
    print(f"Value: {value}")