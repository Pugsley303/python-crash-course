# 8-9 messages page 146

# deine function that prints messages from list
def show_messages(messages):
    for message in messages:
            print(message)

# define list with some text messages
sms = ['Hello World', 'Hello Whirled', 'Hollow World']
sent = []

# call function
show_messages(sms)