# 8-7 album page 142

# define empty dictionaries for the 3 albums    
album_0 = {}
album_1 = {}
album_2 = {}

# define function that makes the album info
def make_album():

# get input from user and populate the appropriate dictionary
    print ("\nLets build some dictionaries using musicians and album titles!\n")
    artist0 = input("What is the 1st artists name? ")
    title0 = input("What is the 1st album title? ")
    album_0[artist0] = title0

    artist1 = input("What is the 2nd artists name? ")
    title1 = input("What is the 2nd albums title? ")
    album_1[artist1] = title1

    artist2 = input("What is the 3rd artists name? ")
    title2 = input("What is the 3rd albums title? ")
    album_2[artist2] = title2

# print out artist & title
    print ("\nYou entered:")
    for key, value in album_0.items():
        print(f" Artist: {key.title()}")
        print(f" Title: {value.title()} \n")
       
    for key, value in album_1.items():
        print(f" Artist: {key.title()}")
        print(f" Title: {value.title()} \n")

    for key, value in album_2.items():
        print(f" Artist: {key.title()}")
        print(f" Title: {value.title()} \n")

def track_numbers():
    print("Album number of tracks information\n")
    tracks_0 = input("Do you know how many tracks are on the 1st album y/n? ")
    if tracks_0 == 'y':
        tracks = input("How many tracks are there on this album? ")
        print(f" You said there are {tracks} tracks on that album.\n")
        album_0.update({'Tracks': "tracks"})
        print(album_0)

    tracks_1 = input("Do you know how many tracks are on the 2nd album y/n? ")
    if tracks_1 == 'y':
        num_tracks_1 = input("How many tracks are there on this album? ")
        print(f" You said there are {num_tracks_1} tracks on that album.\n")
    
    tracks_2 = input("Do you know how many tracks are on the 3rd album y/n? ")
    if tracks_2 == 'y':
        num_tracks_2 = input("How many tracks are there on this album? ")
        print(f" You said there are {num_tracks_2} tracks on that album.\n")

# call function that makes the album
make_album()

# call the function that adds track number info
track_numbers()