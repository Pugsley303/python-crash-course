# 6-7 page 112 people
people_0 = {'first': 'corey', 'last': 'mcdonald', 'age': '50', 'city': 'denver'}
people_1 = {'first': 'ann', 'last': 'smith', 'age': '54', 'city': 'denver'}
people_2 = {'first': 'pugsley', 'last': 'adams', 'age': '50', 'city': 'boulder'}
peeps = [people_0, people_1, people_2]

# this prints out the key , but not needed for this exercise
# for key, value in people_0.items():
#    print(f"{key.title()}")

for key, value in people_0.items():
    print(f"{value.title()}")
print("\n")

for key, value in people_1.items():
    print(f"{value.title()}")
print("\n")

for key, value in people_2.items():
    print(f"{value.title()}")
print("\n")

# 6-8 page 112 pets
pet_0 = {'pet': 'dog', 'owner': 'corey', 'pet_name': 'snoopy'}
pet_1 = {'pet': 'cat', 'owner': 'wednesday', 'pet_name': 'fluffy'}
pet_2 = {'pet': 'fish', 'owner': 'pugsley', 'pet_name': 'swimmy'}
pets = [pet_0, pet_1, pet_2]
for pet in pets:
    print(pet)
print("\n")

# 6-9 page 112 fav places
fav_places_0 = {
    'mom': { 'home', 'cafe'},
    'bob': { 'work', 'shop'},
    'pugsley': { 'coffee', 'work'}
    }

for peep in fav_places_0:
    print(f"{peep.title()} - ")
    for local in fav_places_0[peep]:
        print(f"{local}")