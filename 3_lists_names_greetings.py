# practicing with lists page 36

# 3-1 & 3-2
names = ['Ann', 'Sarah', 'Jaclynn', 'Megan', 'Erin']
print(f"\nHello {names[0]} I am glad to see you.")
print(f"\nHello {names[1]} I am glad to see you.")
print(f"\nHello {names[2]} I am glad to see you.")
print(f"\nHello {names[3]} I am glad to see you.")
print(f"\nHello {names[4]} I am glad to see you.")
print()

# 3-3
transportation = ['car', 'motorcycle', 'bicycle', 'scooter']
print(f"\nOut of some cool modes of transportation; {transportation[0]}, {transportation[1]}, {transportation[2]} and {transportation[3]}, I think {transportation[3]}s are the most fun!")

