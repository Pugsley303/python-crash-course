# 4-10 page 65
animals =  ['cat', 'dog', 'rat', 'hamster', 'rabbit']
for animal in animals:
    print(f"{animal.title()}s are fluffy!")
print("The first 3 animals are: ")
for animal in animals[:3]:
    print(animal)
print("Three animals from the middle of thist are: ")
for animal in animals[1:4]:
    print(animal)
print("The last 3 animals are: ")
for animal in animals[-3:]:
    print(animal)
print("These are all 4 legged animals.")