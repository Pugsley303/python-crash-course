# 5-1 page 78
dog = 'doberman'
if dog == 'doberman':
    print("A dog is a doberman, so this is:")
    print(dog == 'doberman')
cat = 'persian'
if cat == 'persian':
    print("A cat is a persian, so this is:")
    print(cat == 'persian')
if dog != 'persian':
    print("A dog is not a persian, so this is:")
    print(dog != 'doberman')
if cat != 'doberman':
    print("A cat is not a doberman, so this is:")
    print(cat != 'persian')
# 5-2 page 78
number = 5
if number < 6:
    print("number is less than 6")
if number > 4:
    print("number is more than 4")
if number == 5:
    print("number is 5")
supplies = ['pen', 'paper', 'eraser']
if 'pen' in supplies:
    print("pen is in supplies")
if 'pen' and 'paper' in supplies:
    print("pen and paper are in supplies")
if 'book' not in supplies:
    print("book is not in supplies")
if 'book' or 'computer' not in supplies:
    print("book or computer are not in supplies")
if 'book' or 'pen' in supplies:
    print("book or pen is in supplies")