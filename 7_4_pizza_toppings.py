# 7-4 pizza topping page 123
# 7-6 three exits

print("Enter a pizza topping one at a time, type QUIT when done.")
print("Max. 4 toppings per pizza.")

# get the 1st topping
topping = input("Please enter first topping: ")
top_num = 0

# while loop with inf loop prohibitor
while topping != 'quit':
    print(f"I will add {topping} to the pizza. \n")
    top_num = top_num + 1
# use this to test for proper incrementation
# print(top_num)

# Check for max toppings & exit if # reached
    if top_num == 4:
        topping = 'quit'
        print("You have reached max # of toppings.")

# the following line prevents an inf loop and gets the 2nd topping or quits
    else:
        topping = input("Please enter next topping: ")