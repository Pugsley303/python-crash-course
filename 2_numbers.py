# examples from page 29

# math
print(5+3)
print(10-2)
print(2*4)
print(16/2)

# math with varable
fav_number = 7
print(f"Fav # {fav_number}")