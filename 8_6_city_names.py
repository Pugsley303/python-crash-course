# 8-6 city names

# define funtion
def city_country():
    cty_ctry_0 = {
        'vancouver': 'canada',
        'london': 'england',
        'paris': 'france',
        }
    for key, value in cty_ctry_0.items():
        print(f"City: {key.title()}")
        print(f"Country: {value.title()}")

# call function
city_country()