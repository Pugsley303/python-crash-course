# examples from the book page 25

print("Hello World!")
quote = "It s better to have people think you are stupid than open your mouth and remove all doubt."
person = " Mark Twain "
print(f"Famous quote - \n{quote} \n{person}")
print(f"Name sans white space \n")

# stripping white space from the right side
person = person.rstrip()
print(person)

# stripping white space from the left side
person = person.lstrip()
print(person)

# stripping white space from both sides
person = person.strip()
print(person)