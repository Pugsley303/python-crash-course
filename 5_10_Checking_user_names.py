# 5-10 Checking user names page 89

# users lists
current_users = ['admin', 'alpha', 'bravo', 'charlie', 'delta']
new_users = ['alpha', 'echo', 'foxtrot', 'golf', 'hotel']

for new_user in new_users:
    if new_user in current_users:
        print(f"Sorry {new_user} is already in use.")