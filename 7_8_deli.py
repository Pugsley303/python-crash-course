# 7-8 and 7-9 deli page 127

# defining both populated & empty lists
sandwich_orders = ['blt', 'pastrami', 'cheese', 'pastrami', 'pb&j', 'pastrami']
finished_sandwiches = []

#
# here is the 7-9 section that was added
#
print ("Sorry, but no Pastrami today :-( \n ")
while 'pastrami' in sandwich_orders:
    sandwich_orders.remove('pastrami')
#
# end of 7-9 add
#

# move from orders to finished list
print ("Making sandwiches - ")
while sandwich_orders:
    current_sandwich = sandwich_orders.pop()
    print (f" {current_sandwich.title()}")
    finished_sandwiches.append(current_sandwich)

# show finished list is now populated
print ("\nFinished sandwiches - ")
for finished in finished_sandwiches:
    print (f" {finished.title()}")

# prove orders list is empty
print ("\n")
for verify in sandwich_orders:
    print (f" {verify.title()}")