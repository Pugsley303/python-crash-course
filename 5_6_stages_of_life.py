# 5-6 stages of life
age = 65
if age < 2:
    print("less than 2")
elif age < 4:
    print("toddler")
elif age <13:
    print("kid")
elif age < 20:
    print("teenager")
elif age < 65:
    print("adult")
else:
    print("elder")