# 8-10 & 8-11 sending messages page 146

# deine function that prints messages from sms list & moves it to sent list
def send_messages(messages):
    while sms:
        current_msg = sms.pop()
        print (current_msg)
        sent.append(current_msg)

# prove sms list is now empty
    print ("\nSMS list:")
    print (sms)

# prove contents of sent list
    print ("\nSent list:")
    print (sent)
    print ("\n")

# define list with some text messages
sms = ['Hello World', 'Hello Whirled', 'Hollow World']
sent = []

# call function
send_messages(sms)