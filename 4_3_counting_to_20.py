# 4-3 & 4-4 & 4-5 page 60
numbers = list(range(1,1000001))
# print(numbers)
print(type(numbers))
print(min(numbers))
print(max(numbers))
print(sum(numbers))

# 4-6
odd_numbers = list(range(1, 20, 2))
print(odd_numbers)
for odd in odd_numbers:
    print(odd)

# 4-7 numbers by 3's
triple_numbers = list(range(3, 22, 3))
print(triple_numbers)
for triple in triple_numbers:
    print(triple_numbers)
