# 7-2 restaurant seating page 117
num_diners = input("How many people will be dining with us tonight? ")
num_diners = int(num_diners)
if num_diners > 8:
    print("\nYou will need to wait forever.")
else:
    print(f"\nStandby while I look for a table for {num_diners}")