# 4-11 page 65
pizzas = ['pms', 'mushroom', 'pepperoni']
for pizza in pizzas:
    print(pizza)
    print(f"I like {pizza.title()} pizza!")
print("I like pizza!\n")
# copy & create new list
friend_pizzas = pizzas[:]
for fpizza in friend_pizzas:
    print(fpizza)
# add new pizzas to each list
pizzas.append('cheese')
friend_pizzas.append('bbq')
# prove they are diff lists
print(pizzas)
print(friend_pizzas)
# 4-12 page 65
for friend_pizza in friend_pizzas:
    print(friend_pizza)