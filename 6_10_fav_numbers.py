# 6-10 Fav numbers page 112
peepsfavs_0 = {
    'corey': {'0', '1'},
    'ann': {'1', '2'},
    'george': {'2', '3'},
    'edward': {'3', '4'},
    'frank': {'4', '5'},
    }

for peep in peepsfavs_0:
    print(f"{peep.title()} : ")
    for favnum in peepsfavs_0[peep]:
        print(f"{favnum}")
# print(peepsfavs_0)