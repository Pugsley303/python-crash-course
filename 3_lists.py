# practicing with lists page 34

letters = ['a', 'b', 'c', 'd', 'e', 'f']
print(letters)
print(f"\nletter {letters[0]}")
print(f"\nletter {letters[-1]}")
print(f"\nThe 3rd letter of the alphabet is '{letters[2]}'")
print()

# 