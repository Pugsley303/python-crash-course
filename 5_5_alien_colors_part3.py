# 5-5 alien colors part 3
alien_color = 'red'

if alien_color == 'green':
    print("5 pts")
elif alien_color == 'red':
    print("15 pts")
else:
    print("YELLOW! 20 pts")

alien_color = 'green'
if alien_color == 'green':
    print("5 pts")
elif alien_color == 'red':
    print("15 pts")
else:
    print("YELLOW! 20 pts")

alien_color = 'YELLOW'
if alien_color == 'green':
    print("5 pts")
elif alien_color == 'red':
    print("15 pts")
else:
    print("YELLOW! 20 pts")