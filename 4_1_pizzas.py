# 4-1 page 56
pizzas = ['pms', 'mushroom', 'pepperoni']
for pizza in pizzas:
    print(pizza)
    print(f"I like {pizza.title()} pizza!")
print("I like pizza!\n")

# 4-2 page 56
animals =  ['cat', 'dog', 'rat']
for animal in animals:
    print(f"{animal.title()}s are fluffy!")
print("These are all 4 legged animals.")
